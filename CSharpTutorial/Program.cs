﻿using System;

namespace CSharpTutorial
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Enter your name");
            string name = Console.ReadLine();
            Console.WriteLine($"Hello {name}");
        }
    }
}